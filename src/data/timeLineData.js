let experienceData = {
  imageStyle: {
    height: '60px',
    width: '60px',
  },
  flexDirectionStyle: {
    'flex-direction': 'row-reverse',
  },
  a: {
    display: 'none',
  },
  coreData: [
    {
      imageTag: 'IBNC',
      imageSrc:
        'https://media.licdn.com/dms/image/C4E0BAQE7cYZB-PhRkA/company-logo_200_200/0?e=2159024400&v=beta&t=lYbUzt8xiufMOevJWZ1u_9vdYEAVaTIxMG5iEBJo1yw',
      heading: 'IBNC',
      subheading: 'Intern - Networking ',
      timeTaken: '(Jan-2016 to Aug-2016)',
      discription:
        'The configuration of routing protocols: STATIC, RIP, EIGRP & OSPF. Experience of Cisco routers backup, recovery & password breaking. Implementing LAN connections using Switches and Routers. Setup of layer 2 switching & STP. Configuring and troubleshooting RIP, EIGRP, OSPF, VLAN, VPN, TRUNKING & IP addressing issues.',
    },
    {
      imageTag: 'AbeRuk',
      imageSrc: 'https://mysite-sachins.s3.ap-south-1.amazonaws.com/aberuk.webp',
      heading: 'AbeRuk',
      subheading: 'Intern - Web Developer',
      timeTaken: '(Aug-2016 to Dec-2016)',
      discription:
        'My time here involved building a simple landing page. This project involved working with adding image, links, lists and paragraphs. This project required knowledge of HTML to create and use of CSS to make the project look better.',
    },
    {
      imageTag: 'TheCloseCompany',
      imageSrc: 'https://mysite-sachins.s3.ap-south-1.amazonaws.com/download.png',
      heading: 'The Close Company',
      subheading: 'Software Developer',
      timeTaken: '(July-2020 to Dec-2020)',
      discription:
        'I was in charge of handling the complete front-end of the company, I handled multiple projects built using React and Redux. Built REDIS configuration for existing MONGO DB configuration. Theme based integrations for different merchants, improving user experience in turn leading to 30% increase in successful transactions.',
    },
    {
      imageTag: 'Neosoft Technologies',
      imageSrc: 'https://mysite-sachins.s3.ap-south-1.amazonaws.com/neosoft.webp',
      heading: 'NeoSoft Technologies',
      subheading: 'Senior Software Developer',
      timeTaken: '(Dec-2020 to July-2022)',
      discription:
        'I was in charge of handling the complete front-end of the company, I handled multiple projects built using React and Redux. Built REDIS configuration for existing MONGO DB configuration. Theme based integrations for different merchants, improving user experience in turn leading to 30% increase in successful transactions.',
    },
    {
      imageTag: 'Dabadu.ai',
      imageSrc: 'https://mysite-sachins.s3.ap-south-1.amazonaws.com/dabadu.png',
      heading: 'Dabadu.ai',
      subheading: 'Senior Software Developer',
      timeTaken: '(Aug-2022 to Present)',
      discription:
        'I was in charge of handling the complete front-end of the company, I handled multiple projects built using React and Redux. Built REDIS configuration for existing MONGO DB configuration. Theme based integrations for different merchants, improving user experience in turn leading to 30% increase in successful transactions.',
    },
  ],
};

let projectData = {
  imageStyle: {
    height: '75%',
    width: '75%',
  },
  imageContainerStyle: {
    'margin-top': '20px',
    'margin-left': '20px',
  },
  flexDirectionStyle: {
    'flex-direction': 'column',
  },
  coreData: [
    {
      link: 'https://reactcrudblog.netlify.app/',
      imageTag: 'CRUD BLOG',
      imageSrc:
        'https://res.cloudinary.com/dssrr585t/image/upload/v1588796123/Screenshot_2020-05-07_at_1.13.45_AM_fhctik.png',
      heading: 'React BLOG',
      subheading: 'Simple Blog app built using React and Redux its a CURD app.',
      discription: 'Tech Stack : [ReactJS, SASS, Bootstrap]',
      bitlink: 'https://bitbucket.org/sachin-sakri/react-blog/src/master/',
    },
    {
      link: 'https://reacttodocrud.netlify.app/',
      imageTag: 'CRUD TODO',
      imageSrc:
        'https://res.cloudinary.com/dssrr585t/image/upload/v1588790388/Screenshot_2020-05-07_at_12.09.18_AM_acsprd.png',
      heading: 'React TODO',
      subheading: 'Simple TODO app built using React and Redux its a CURD app.',
      discription: 'Tech Stack : [ReactJS, Redux, SASS, Bootstrap]',
      bitlink: 'https://bitbucket.org/sachin-sakri/react-todo/src/master/',
    },
    {
      link: 'https://react-redux-trello-clone.netlify.app/',
      imageTag: 'Trello-Clone',
      imageSrc:
        'https://res.cloudinary.com/dssrr585t/image/upload/v1588873079/Screenshot_2020-05-07_at_11.07.37_PM_oyjha5.png',
      heading: 'Trello-Clone',
      subheading: 'A simple cloning version of Trello, using ReactJS.',
      discription: 'Tech Stack : [ReactJS, Redux, SASS, Bootstrap]',
      bitlink: 'https://bitbucket.org/sachin-sakri/react-trello-clone/src/master/',
    },
    {
      link: 'https://react-netflix-clone.netlify.app/',
      imageTag: 'Netflix-Clone',
      imageSrc:
        'https://res.cloudinary.com/dssrr585t/image/upload/v1588875596/Screenshot_2020-05-07_at_11.48.37_PM_cxgxgv.png',
      heading: 'Netflix-Clone',
      subheading: 'A simple cloning version of Netflix, using ReactJS.',
      discription: 'Tech Stack : [ReactJS, Redux, SASS, Bootstrap]',
      bitlink: 'https://bitbucket.org/sachin-sakri/react-netflix-clone/src/master/',
    },
    {
      link: 'https://out-reach.herokuapp.com/homepage_guest',
      imageTag: 'Out-reach',
      imageSrc: 'https://res.cloudinary.com/dssrr585t/image/upload/v1588782895/outreach_qtr3cp.png',
      heading: 'Outreach',
      subheading:
        'Outreach is Platform built for enjoying the new city. Who are you? What is going to make you smile? What is going to make you thrive? Whoever you are, you ever you are going to be, we have got an experience for you. Seize what the city has to offer. Get ready for the fun. It was also built to allow a client to outsource their daily chores, errands, or small jobs. At the same time, it allows those who are unemployed, studying or in between jobs to earn an income in a dignified manner.',
      discription: 'Tech Stack : [ExpressJS, NodeJS, HBS, MongoDB, Mongoose, CSS, Cloudinary]',
      bitlink: 'https://bitbucket.org/sachin-sakri/out-reach/src/master/',
    },
    {
      link: 'https://mern-crud-forum.herokuapp.com/',
      imageTag: 'iDeotic Forum',
      imageSrc: 'https://sachins-site.s3.ap-south-1.amazonaws.com/scrnli_13_05_2020_16-48-49.png',
      heading: 'MERN FORUM',
      subheading: 'Built CRUD Forum app for an Inteview Task. Built with JWT Authentication in Backend',
      discription:
        'Tech Stack : [ExpressJS, NodeJS, HBS, MongoDB, Mongoose, CSS, Cloudinary, JWT Token, Bycrpt, Heroku]',
      bitlink: 'https://bitbucket.org/sachin-sakri/ideotic-forum/src/master/',
    },
  ],
};

export { experienceData, projectData };
