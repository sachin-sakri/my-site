import React, { Component } from 'react';
import './bioComponent.css';
import locationSVG from '../static/location.svg';
import email from '../static/email.svg';
import telephone from '../static/telephone.svg';
class BioComponent extends Component {
  render() {
    return (
      <div>
        <h3>Sachin Sakri</h3>
        <img className='Calendar-symbol' src={email} alt='Email Symbol'></img>{' '}
        <div className='Location'>
          <a href='mailto:sachinssakri@gmail.com'>sachinssakri@gmail.com</a>
        </div>
        <br></br>
        <img className='Location-symbol' src={telephone} alt='Telephone Symbol'></img>{' '}
        <div className='Location'>+91 9008810131</div>
        <p>
          A Full stack developer, a Writer and a Teacher on a mission to improve the world for all humans. Productive
          collaborator in great teams utilizing hard skills to make awesome products and software services.
        </p>
        <img className='Location-symbol' src={locationSVG} alt='location symbol'></img>{' '}
        <div className='Location'>Bangalore</div>
      </div>
    );
  }
}

export default BioComponent;
