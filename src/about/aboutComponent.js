import React, { Component } from 'react';
import './aboutComponent.css';

class AboutComponent extends Component {
	render() {
		return (
			<div class='About'>
				<h3> About Me</h3>
				<p>
					I am a full stack developer. I know JavaScript, Python. Although, I am
					tyring to become a language agnostic person with time.
				</p>
				<p>
					Dedicated and efficient full stack developer with excellence in MERN
					and PERN stack. I have exceptional communication skills to work with
					other engineers alongside. I understand the importance of working in a
					team. I have accepted that I am a flawed human being and I will make
					mistakes, but I will assure you that no mistakes will be made during
					the production phase.
				</p>
				<p>
					I just want to answer a question that many ask me, "Why did you choose
					Web Devlopment?"
					<br></br>
					Web Developement now a days is an art, we do so much more than just
					make a website. We build complex programs and software that can be
					implemented on the backend of websites, the parts that people can't
					see.
					<br></br>
					We're artists we make everything web functionally beautiful.
				</p>
				<p>
					My interest vary on long spectrum, I am fascinated by ML and AI. I am
					working on my skills to reach the peak of this field. I got more
					fascinated after doing a little research - Artificial Intelligence
					seemed little about simulating human brain and more about refined
					mathematical models with solid reasoning about their working.
				</p>
				<p>
					In the same way that physicists look for a unified model, I view
					machine learning as the search for a unified algorithm. It is an
					attempt to solve problems generically by designing a reasoning
					algorithm, and I like the elegance in that. It is probably one of the
					more practical uses for applied math and statistics.
				</p>

				<h3 className='Links'>Links</h3>
				<ul>
					<li>
						<a href='https://mysite-sachins.s3.ap-south-1.amazonaws.com/Sachin.pdf'>Resume</a>
					</li>
					<li>
						<a href='https://angel.co/u/sachin-sakri'>AngelList</a>
					</li>
					<li>
						<a href='https://github.com/sachin-sakri-au5'>Github</a>
					</li>
					<li>
						<a href='https://twitter.com/sachin_sakri'>Twitter</a>
					</li>
					<li>
						<a href='https://www.linkedin.com/in/sachin-sakri-4ba82910a/'>
							LinkedIn
						</a>
					</li>
					<li>
						<a href='https://bitbucket.org/sachin-sakri/'>BitBucket</a>
					</li>
				</ul>
				<p>
					PS:{` `} I use BitBucket for my personal projects. My GitHub consists
					of Work Related projects and are not Public. <span>🖥</span>
				</p>
			</div>
		);
	}
}

export default AboutComponent;
