import React, { Component } from 'react';

import './blogComponent.css';

class BlogComponent extends Component {
	render() {
		return (
			<div>
				<div className='Element-container'>
					<div className='Text-container'>
						<a
							href='https://medium.com/@sachinssakri/linear-search-in-python-6d62869e586f'
							target='_blank'
							rel='noopener noreferrer'
						>
							<h3 className='element-heading'>Linear Search in Python</h3>
						</a>
						<p>
							We’ll discuss a simple way to perform Linear Search in a given
							List (Array) using Python.
						</p>
						<time>
							7 May 2020 .<span> 1 Min Read</span>
						</time>
					</div>
				</div>
                <div className='Element-container'>
					<div className='Text-container'>
						<a
							href='https://www.notion.so/sachinssakri/Coding-Guidelines-React-e1c4fec01d91468da7b8b610121c28a1'
							target='_blank'
							rel='noopener noreferrer'
						>
							<h3 className='element-heading'>Prmoises Async and Await</h3>
						</a>
						<p>
							We will discuss about JavaScript Prmoises and discuss Async Await.
                            Asynchronous concept in JavaScript is what sets this aside from everything.
						</p>
						<time>
							1 June 2020 .<span> 10 Min Read</span>
						</time>
					</div>
				</div>
			</div>
		);
	}
}

export default BlogComponent;