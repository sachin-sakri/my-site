import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import './homeComponent.css';
import NavBarComponent from '../navbar/navbarComponent';
import BioComponent from '../bio/bioComponent';
import ExperienceComponent from '../experience/experienceComponent';
import BlogComponent from '../blog/blogComponent';
import cover from '../static/cover.jpg';
import { experienceData, projectData } from '../data/timeLineData';

import AboutComponent from '../about/aboutComponent';
class HomeComponent extends Component {
	render() {
		return (
			<div className='Container'>
				<img className='Cover-photo' src={cover} alt='Desk'></img>
				<img
					className='Profile-pic'
					src='https://mysite-sachins.s3.ap-south-1.amazonaws.com/ProfilePic.jpg'
					alt='My profile pic'
				></img>
				<article className='Article'>
					<NavBarComponent forceUpdate={this.forceUpdate}></NavBarComponent>
					<div className='MainContent'>
						<div className='Bio'>
							<BioComponent></BioComponent>
						</div>
						<div className='Time-line' id='Time-line'>
							<Switch>
								<Route
									exact
									path={'/experience'}
									render={(props) => (
										<ExperienceComponent {...props} data={experienceData} />
									)}
								/>
								<Route
									exact
									path={'/projects'}
									render={(props) => (
										<ExperienceComponent {...props} data={projectData} />
									)}
								/>
								<Route exact path={'/about'} component={AboutComponent} />
								<Route exact path={'/blog'} component={BlogComponent} />
								<Route
									exact
									path={'/'}
									render={() => <Redirect to='/about' />}
								/>
							</Switch>
						</div>
					</div>
				</article>
			</div>
		);
	}
}

export default HomeComponent;
